package uk.co.unclealex.mongodb.bson

import org.bson.types.Decimal128
import uk.co.unclealex.mongodb.bson.BsonDocumentCodec

import java.time.Instant

case class Numbers(i: Int, l: Long, d: Double, dec: Decimal128, in: Instant)
    derives BsonDocumentCodec
