package uk.co.unclealex.mongodb.bson

import uk.co.unclealex.mongodb.bson.BsonDocumentCodec;

sealed trait Options derives BsonDocumentCodec

case class OneOption(foo: String) extends Options derives BsonDocumentCodec
case class AnotherOption(bar: String) extends Options derives BsonDocumentCodec
