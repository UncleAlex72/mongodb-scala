package uk.co.unclealex.mongodb.macros

import org.scalatest.matchers.should
import org.scalatest.wordspec.AnyWordSpec

class ChangeValueTest extends AnyWordSpec with should.Matchers:

  case class Animal(name: String, species: String)

  "The change value macro" should {
    "change a value in a case class" in:
      val pebbles = Animal(name = "Pebbles", species = "cat")
      val result = ChangeValue(pebbles, "species", "terror")
      result shouldEqual Animal(name = "Pebbles", species = "terror")
  }
