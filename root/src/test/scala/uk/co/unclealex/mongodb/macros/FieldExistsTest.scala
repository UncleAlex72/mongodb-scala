package uk.co.unclealex.mongodb.macros

import org.scalatest.matchers.should
import org.scalatest.wordspec.AnyWordSpec

class FieldExistsTest extends AnyWordSpec with should.Matchers {

  case class Instrument(name: String, tpe: String)
  case class Musician(name: String, instrument: Instrument)

  "The field exists macro" should {
    "be able to find a single field" in:
      FieldExists.fieldExists[Musician]("name") shouldEqual Right("name")

    "be able to find a nested field" in:
      FieldExists.fieldExists[Musician]("instrument.tpe") shouldEqual Right(
        "instrument.tpe"
      )

    "throw an error when the field does not exist" in:
      FieldExists.fieldExists[Musician]("nom") shouldEqual Left(
        "Class Musician does not contain a field called nom"
      )

    "throw an error when a nested field does not exist" in:
      FieldExists.fieldExists[Musician]("instrument.nom") shouldEqual Left(
        "Class Instrument does not contain a field called nom"
      )
  }
}
