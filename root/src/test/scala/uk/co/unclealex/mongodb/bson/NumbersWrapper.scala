package uk.co.unclealex.mongodb.bson

import uk.co.unclealex.mongodb.bson.{BsonDocumentCodec, ID}

case class NumbersWrapper(
    id: ID,
    str: String,
    bool: Boolean,
    maybe: Option[String],
    maybeNot: Option[String],
    numbers: Numbers,
    strs: Seq[String]
) derives BsonDocumentCodec
