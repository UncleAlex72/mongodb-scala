package uk.co.unclealex.mongodb.bson

import org.bson.types.{Decimal128, ObjectId}
import org.scalatest.matchers.should
import org.scalatest.wordspec.AsyncWordSpec
import uk.co.unclealex.mongodb.bson.*

import java.time.Instant
import scala.util.{Failure, Success}

class BsonDocumentCodecSpec extends AsyncWordSpec with should.Matchers:

  val now: Instant = Instant.now()

  val wrapper: NumbersWrapper = NumbersWrapper(
    id = ID("1234abcd56781234abcd5678"),
    str = "Hello",
    bool = false,
    maybe = Some("maybe"),
    maybeNot = None,
    numbers = Numbers(
      i = 1,
      l = 2,
      d = 3.0,
      dec = Decimal128.parse("128"),
      in = now
    ),
    strs = Seq("one", "two", "three")
  )

  val wrapperDocument: BsonDocument = BsonDocument(
    "maybe" -> BsonString("maybe"),
    "strs" -> BsonArray(
      Seq(BsonString("one"), BsonString("two"), BsonString("three"))
    ),
    "bool" -> BsonBoolean.FALSE,
    "maybeNot" -> BsonNull,
    "numbers" ->
      BsonDocument(
        "in" -> BsonDateTime(now),
        "i" -> BsonInt32(1),
        "l" -> BsonInt64(2),
        "dec" -> BsonDecimal128(Decimal128.parse("128")),
        "d" -> BsonDouble(3.0)
      ),
    "str" -> BsonString("Hello"),
    "id" -> BsonObjectId(new ObjectId("1234abcd56781234abcd5678"))
  )

  val anOption: Options = OneOption("foo")

  val optionsDocument: BsonDocument = BsonDocument(
    "foo" -> BsonString("foo"),
    "className" -> BsonString("OneOption")
  )

  "The bson document codec" should {
    "correctly write a case class" in:
      summon[BsonDocumentCodec[NumbersWrapper]].writeDocument(
        wrapper
      ) shouldEqual wrapperDocument

    "correct write a sum class" in:
      summon[BsonDocumentCodec[Options]].writeDocument(
        anOption
      ) shouldEqual optionsDocument

    "correctly read a case class" in:
      summon[BsonDocumentCodec[NumbersWrapper]].readDocument(
        wrapperDocument
      ) shouldEqual Success(wrapper)

    "ignore extra fields" in:
      summon[BsonDocumentCodec[NumbersWrapper]].readDocument(
        wrapperDocument + ("foo" -> BsonString("foo"))
      ) shouldEqual Success(wrapper)

    "correct read a sum class" in:
      summon[BsonDocumentCodec[Options]].readDocument(
        optionsDocument
      ) shouldEqual Success(anOption)

    "fail if a required field in a case class is missing" in:
      summon[BsonDocumentCodec[NumbersWrapper]].readDocument(
        wrapperDocument - "bool"
      ) match
        case Success(value) => fail("A missing field should not be allowed")
        case Failure(_)     => succeed

    "fail if a the discriminator field for a sum class is missing" in:
      summon[BsonDocumentCodec[Options]].readDocument(
        optionsDocument - "className"
      ) match
        case Success(value) => fail("A discriminator is required")
        case Failure(_)     => succeed

    "fail if a the discriminator field for a sum class does not match" in:
      summon[BsonDocumentCodec[Options]].readDocument(
        optionsDocument + ("className" -> BsonString("bunny"))
      ) match
        case Success(value) => fail("A discriminator must match")
        case Failure(_)     => succeed

    "fail if a the discriminator field for a sum class is not a string" in:
      summon[BsonDocumentCodec[Options]].readDocument(
        optionsDocument + ("className" -> BsonInt32(1))
      ) match
        case Success(value) => fail("A discriminator must match")
        case Failure(_)     => succeed
  }
