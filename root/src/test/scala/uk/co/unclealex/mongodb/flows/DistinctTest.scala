package uk.co.unclealex.mongodb.test.flows

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.{Sink, Source}
import org.scalatest.matchers.should
import org.scalatest.wordspec.AsyncWordSpec
import uk.co.unclealex.mongodb.flows.Distinct

class DistinctTest extends AsyncWordSpec with should.Matchers:

  given actorSystem: ActorSystem = ActorSystem()

  "The distinct flow" should {
    "remove duplicated elements" in:
      val eventualResults =
        Source(Seq(1, 2, 2, 2, 3, 4, 5, 5)).via(Distinct.flow).runWith(Sink.seq)
      eventualResults.map: results =>
        results should contain theSameElementsInOrderAs Seq(1, 2, 3, 4, 5)
  }
