package uk.co.unclealex.mongodb.persistable

import shapeless3.deriving.K0
import uk.co.unclealex.mongodb.bson.{BsonDocument, BsonDocumentCodec, ID}
import uk.co.unclealex.mongodb.macros.ChangeValue

import scala.reflect.Selectable.reflectiveSelectable
import scala.util.Try

trait Persistable[A] extends BsonDocumentCodec[A]:

  extension (a: A)
    def _id: Option[ID]
    def withId(newId: ID): A

class DerivedPersistable[A <: { val _id: Option[ID] }](
    codec: BsonDocumentCodec[A],
    idFn: A => Option[ID],
    withIdFn: A => ID => A
) extends Persistable[A]:
  extension (a: A)
    override def _id: Option[ID] = idFn(a)
    override def withId(newId: ID): A = withIdFn(a)(newId)

  override def readDocument(doc: BsonDocument): Try[A] =
    codec.readDocument(doc)

  override def writeDocument(t: A): BsonDocument = codec.writeDocument(t)

object Persistable:
  inline def derived[A <: { val _id: Option[ID] }](using
      gen: K0.Generic[A]
  ): Persistable[A] =
    val codec: BsonDocumentCodec[A] = BsonDocumentCodec.derived[A]
    val idFn: A => Option[ID] = _._id
    val withIdFn: A => ID => A = a =>
      newId => ChangeValue(a, "_id", Some(newId))
    new DerivedPersistable[A](codec, idFn, withIdFn)
