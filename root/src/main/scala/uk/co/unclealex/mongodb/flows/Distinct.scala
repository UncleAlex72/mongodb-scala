package uk.co.unclealex.mongodb.flows

import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.Flow

object Distinct:

  def flow[A]: Flow[A, A, NotUsed] =
    def mapper(state: Option[A], el: A): (Option[A], Option[A]) =
      val newEl: Option[A] = if (state.contains(el)) None else Some(el)
      (Some(el), newEl)
    Flow[A]
      .statefulMap[Option[A], Option[A]](() => None)(mapper, _ => None)
      .mapConcat(identity)
