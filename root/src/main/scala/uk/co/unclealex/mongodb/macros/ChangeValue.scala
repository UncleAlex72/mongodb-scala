package uk.co.unclealex.mongodb.macros

import scala.quoted.*

/** A macro that can change a field in a case class where the field is supplied
  * as a string value.
  */
object ChangeValue:
  inline def apply[T, V](
      inline instance: T,
      fieldName: String,
      newValue: V
  ): T = ${ changeValueImpl[T, V]('instance, 'fieldName, 'newValue) }

  private def changeValueImpl[T: Type, V: Type](
      instance: Expr[T],
      fieldNameExpr: Expr[String],
      newValue: Expr[V]
  )(using q: Quotes): Expr[T] =
    import q.reflect.*

    val sym = TypeRepr.of[T].typeSymbol
    val fieldName = fieldNameExpr.valueOrAbort

    val constructor = sym.primaryConstructor

    if (sym.declaredFields.find(_.name == fieldName).isEmpty)
      report.errorAndAbort(
        s"Case class ${sym.name} does not have a field named $fieldName"
      )

    val args = constructor.paramSymss.flatten.map: param =>
      if (param.name == fieldName) newValue.asTerm
      else Select.unique(instance.asTerm, param.name).asExpr.asTerm

    val newInstance = Apply(Select(New(TypeIdent(sym)), constructor), args)

    newInstance.asExprOf[T]
