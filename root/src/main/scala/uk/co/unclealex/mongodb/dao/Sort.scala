package uk.co.unclealex.mongodb.dao

import com.mongodb.client.model.Sorts.{ascending, descending}
import org.bson.conversions.Bson
import uk.co.unclealex.mongodb.bson.{BsonDocument, BsonValue}
import uk.co.unclealex.mongodb.dao.SortType.{Ascending, Descending}

case class Sort(sortTypesByColumn: Map[String, SortType]):
  private def asBson: Bson =
    sortTypesByColumn
      .foldLeft(BsonDocument()):
        case (doc, (field, sortType)) =>
          val thisSort = sortType match
            case Ascending  => ascending(field)
            case Descending => descending(field)
          doc.merge(BsonValue(thisSort.toBsonDocument))
      .asJava

enum SortType:
  case Ascending
  case Descending

object Sort:

  given Conversion[Sort, Bson] = _.asBson
