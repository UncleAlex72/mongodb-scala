package uk.co.unclealex.mongodb.bson

import shapeless3.deriving.{K0, Labelling}
import BsonDocument.~>

trait BsonDocumentWriter[A] extends BsonWriter[A]:

  def writeDocument(a: A): BsonDocument

  override def write(a: A): BsonValue = writeDocument(a)

object BsonDocumentWriter:

  private def deriveBsonWriterProduct[A](using
      pInst: K0.ProductInstances[BsonWriter, A],
      labelling: Labelling[A]
  ): BsonDocumentWriter[A] =
    (a: A) =>
      val empty: Map[String, BsonValue] = Map.empty
      val elements = labelling.elemLabels.zipWithIndex.foldLeft(empty):
        case (acc, (label, index)) =>
          val value = pInst.project(a)(index)(
            [t] => (writer: BsonWriter[t], pt: t) => writer.write(pt)
          )
          acc + (label -> value)
      BsonDocument(elements)

  private def deriveBsonWriterSum[A](using
      cInst: K0.CoproductInstances[BsonWriter, A]
  ): BsonDocumentWriter[A] =
    (a: A) =>
      cInst.fold(a)(
        [T] =>
          (st: BsonWriter[T], t: T) =>
            val bson = st.write(t)
            bson match
              case bd: BsonDocument =>
                bd + ("className" ~> t.getClass.getSimpleName)
              case bson => BsonDocument("value" -> bson)
      )
  inline def derived[A](using gen: K0.Generic[A]): BsonDocumentWriter[A] =
    gen.derive(deriveBsonWriterProduct, deriveBsonWriterSum)

  given BsonDocumentWriter[BsonDocument] with
    override def writeDocument(a: BsonDocument): BsonDocument = a
