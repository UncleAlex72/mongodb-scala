package uk.co.unclealex.mongodb.dao

trait IndexDsl[A]:

  val index: Index[A] = Index[A]()
