package uk.co.unclealex.mongodb.macros

import scala.annotation.tailrec
import scala.quoted.*

/** A macro that will cause a compilation error if a string is not a valid case
  * class field name.
  */
object FieldExists:

  inline def fieldExists[T](inline fieldName: String): Either[String, String] =
    ${ fieldExistsImpl[T]('fieldName) }

  private def fieldExistsImpl[T: Type](fieldNameExpr: Expr[String])(using
      q: Quotes
  ): Expr[Either[String, String]] =
    import q.reflect.*

    fieldNameExpr.value match
      case None => '{ Left("You must supply a string literal") }
      case Some(fullFieldName) =>
        if (fullFieldName.isEmpty) '{ Left("A field name cannot be empty") }
        else
          @tailrec
          def checkFieldsRecursively(
              tpe: TypeRepr,
              fieldNames: List[String]
          ): Either[String, String] =
            fieldNames match
              case Nil => Left("The field nesting is too long")
              case fieldName :: tail =>
                val fieldSyms = tpe.typeSymbol.caseFields
                fieldSyms.find(_.name == fieldName) match
                  case None =>
                    Left(
                      s"Class ${tpe.typeSymbol.name} does not contain a field called $fieldName"
                    )
                  case Some(fieldSym) =>
                    if (tail.isEmpty) Right(fullFieldName)
                    else
                      val fieldType = tpe.memberType(fieldSym)
                      val typeSymbol = fieldType.typeSymbol
                      if (
                        typeSymbol.isClassDef && typeSymbol.flags.is(Flags.Case)
                      )
                        checkFieldsRecursively(fieldType, tail)
                      else
                        Left(s"Class ${fieldType.show} is not a case class")

          val typeRepr = TypeRepr.of[T]
          val fieldNames = fullFieldName.split('.').toList
          Expr(checkFieldsRecursively(typeRepr, fieldNames))

  inline def apply[T](inline fieldName: String): String =
    ${ applyImpl[T]('fieldName) }

  private def applyImpl[T: Type](fieldNameExpr: Expr[String])(using
      q: Quotes
  ): Expr[String] =
    fieldExistsImpl[T](fieldNameExpr).valueOrAbort match
      case Left(msg)        => q.reflect.report.errorAndAbort(msg)
      case Right(fieldName) => Expr(fieldName)
