package uk.co.unclealex.mongodb.dao

import Query.{compare, compareBson}
import uk.co.unclealex.mongodb.bson.{
  BsonArray,
  BsonBoolean,
  BsonNull,
  BsonWriter
}
import scala.annotation.targetName

trait QueryDSL[A]:

  extension (field: String)

    private def elvis[C](maybeValue: Option[C], compare: C => Query): Query =
      maybeValue.map(compare).getOrElse(Query.all)

    @targetName("isEqualTo")
    inline def $eq[O](value: O)(using BsonWriter[O]): Query =
      compare[A, O](field, "$eq", value)
    @targetName("isEqualToInfix")
    inline infix def ===[O](value: O)(using BsonWriter[O]): Query = $eq(
      value
    )

    @targetName("isNotEqualTo")
    inline def $ne[O](value: O)(using BsonWriter[O]): Query =
      compare[A, O](field, "$ne", value)
    @targetName("isNotEqualToInfix")
    inline infix def !==[O](value: O)(using BsonWriter[O]): Query = $ne(
      value
    )

    @targetName("isGreaterThan")
    inline def $gt[C](value: C)(using BsonWriter[C]): Query =
      compare[A, C](field, "$gt", value)
    @targetName("isGreaterThanInfix")
    inline infix def >[C](value: C)(using BsonWriter[C]): Query = $gt(value)

    @targetName("isEqualToOrGreaterThan")
    inline def $gte[C](value: C)(using BsonWriter[C]): Query =
      compare[A, C](field, "$gte", value)
    @targetName("isEqualToOrGreaterThanInfix")
    inline infix def >=[C](value: C)(using BsonWriter[C]): Query = $gte(
      value
    )

    @targetName("isLessThan")
    inline def $lt[C](value: C)(using BsonWriter[C]): Query =
      compare[A, C](field, "$lt", value)
    @targetName("isLessThanInfix")
    inline infix def <[C](value: C)(using BsonWriter[C]): Query = $lt(value)

    @targetName("isLessThanOrEqualTo")
    inline def $lte[C](value: C)(using BsonWriter[C]): Query =
      compare[A, C](field, "$lte", value)
    @targetName("isLessThanOrEqualToInfix")
    inline infix def <=[C](value: C)(using BsonWriter[C]): Query = $lte(
      value
    )

    @targetName("isGreaterThanIfDefined")
    inline infix def ?>[C](maybeValue: Option[C])(using BsonWriter[C]): Query =
      elvis(maybeValue, (v: C) => >(v))
    @targetName("isEqualToOrGreaterThanIfDefined")
    inline infix def ?>=[C](maybeValue: Option[C])(using BsonWriter[C]): Query =
      elvis(maybeValue, (v: C) => >=(v))
    @targetName("isLessThanIfDefined")
    inline infix def ?<[C](maybeValue: Option[C])(using BsonWriter[C]): Query =
      elvis(maybeValue, (v: C) => <(v))
    @targetName("isLessThanOrEqualToIfDefined")
    inline infix def ?<=[C](maybeValue: Option[C])(using BsonWriter[C]): Query =
      elvis(maybeValue, (v: C) => <=(v))

    // between
    @targetName("betweenInclusiveInclusive")
    inline def ?>=&<=[C](
        maybeLowerBound: Option[C],
        maybeUpperBound: Option[C]
    )(using
        BsonWriter[C]
    ): Query =
      ?>=(maybeLowerBound) && ?<=(maybeUpperBound)

    @targetName("betweenInclusiveExclusive")
    inline def ?>=&<[C](maybeLowerBound: Option[C], maybeUpperBound: Option[C])(
        using BsonWriter[C]
    ): Query =
      ?>=(maybeLowerBound) && ?<(maybeUpperBound)

    @targetName("betweenExclusiveInclusive")
    inline def ?>&<=[C](maybeLowerBound: Option[C], maybeUpperBound: Option[C])(
        using BsonWriter[C]
    ): Query =
      ?>(maybeLowerBound) && ?<=(maybeUpperBound)

    @targetName("betweenExclusiveExclusive")
    inline def ?>&<[C](maybeLowerBound: Option[C], maybeUpperBound: Option[C])(
        using BsonWriter[C]
    ): Query =
      ?>(maybeLowerBound) && ?<(maybeUpperBound)

    inline def isEmpty: Query = compareBson[A](field, "$eq", BsonArray())
    inline def isNotEmpty: Query = compareBson[A](field, "$ne", BsonArray())

    inline def isNull: Query =
      compareBson[A](field, "$eq", BsonNull)

    inline def isNotNull: Query =
      compareBson[A](field, "$ne", BsonNull)

    inline def doesExist: Query =
      compareBson[A](field, "$exists", BsonBoolean.TRUE) && isNotNull
