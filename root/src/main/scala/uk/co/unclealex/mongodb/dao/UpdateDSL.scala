package uk.co.unclealex.mongodb.dao

import org.bson.BsonValue
import uk.co.unclealex.mongodb.bson.BsonDocument.~>
import uk.co.unclealex.mongodb.bson.{BsonDocument, BsonWriter}
import uk.co.unclealex.mongodb.macros.FieldExists

trait UpdateDSL[A]:

  inline def $set[V](field: String, value: V)(using
      BsonWriter[V]
  ): UpdateBuilder =
    UpdateBuilder(BsonDocument(field ~> value))

  case class UpdateBuilder(bsonDocument: BsonDocument):
    def build: Update = Update.$set(bsonDocument)

    inline def and[V](field: String, value: V)(using
        BsonWriter[V]
    ): UpdateBuilder =
      UpdateBuilder(bsonDocument + (FieldExists[A](field) ~> value))

  given Conversion[UpdateBuilder, Update] = _.build
