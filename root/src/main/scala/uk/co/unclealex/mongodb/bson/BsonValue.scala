package uk.co.unclealex.mongodb.bson

import com.mongodb.MongoClientSettings
import org.bson
import org.bson.codecs.configuration.{CodecRegistries, CodecRegistry}
import org.bson.codecs.{BsonValueCodec, Codec, DecoderContext, EncoderContext}
import org.bson.types.{Decimal128, ObjectId}
import org.bson.{
  BsonDocument,
  BsonValue,
  codecs,
  BsonArray as JavaBsonArray,
  BsonBoolean as JavaBsonBoolean,
  BsonDateTime as JavaBsonDateTime,
  BsonDecimal128 as JavaBsonDecimal128,
  BsonDocument as JavaBsonDocument,
  BsonDouble as JavaBsonDouble,
  BsonInt32 as JavaBsonInt32,
  BsonInt64 as JavaBsonInt64,
  BsonNull as JavaBsonNull,
  BsonObjectId as JavaBsonObjectId,
  BsonString as JavaBsonString,
  BsonValue as JavaBsonValue
}
import uk.co.unclealex.mongodb.*

import java.time.Instant
import scala.annotation.targetName
import scala.jdk.javaapi.CollectionConverters

sealed trait BsonValue:
  def asJava: JavaBsonValue

case class BsonArray(values: Seq[BsonValue] = Seq.empty) extends BsonValue:
  override def asJava: JavaBsonArray =
    val arr = new JavaBsonArray()
    arr.addAll(CollectionConverters.asJava(values.map(_.asJava)))
    arr

case class BsonDocument(fields: Map[String, BsonValue] = Map.empty)
    extends BsonValue:
  override def asJava: JavaBsonDocument =
    val doc = new JavaBsonDocument()
    doc.putAll(
      CollectionConverters.asJava(fields.view.mapValues(_.asJava).toMap)
    )
    doc

  @targetName("withField")
  infix def +(kv: (String, BsonValue)): BsonDocument =
    copy(fields = fields + kv)

  @targetName("withoutField")
  infix def -(kv: String): BsonDocument =
    copy(fields = fields - kv)

  def get(key: String): Option[BsonValue] = fields.get(key)

  def merge(a: BsonValue): BsonDocument =
    def _merge(a: BsonValue, b: BsonDocument): BsonDocument =
      (a, b) match
        case (oa: BsonDocument, ob: BsonDocument) =>
          ob.fields.foldLeft(oa):
            case (result, (key, value)) =>
              val newValue: BsonValue = (result.get(key), value) match
                case (Some(sub: BsonDocument), ov: BsonDocument) =>
                  _merge(sub, ov)
                case _ => value

              result + (key -> newValue)
        case _ => b

    _merge(a, this)

object BsonDocument:
  def apply(kv: (String, BsonValue)): BsonDocument = BsonDocument(Seq(kv).toMap)
  def apply(kv: (String, BsonValue), kvs: (String, BsonValue)*): BsonDocument =
    BsonDocument((kv +: kvs).toMap)

  extension (str: String)
    @targetName("withFieldAndWriter")
    infix def ~>[A](value: A)(using BsonWriter[A]): (String, BsonValue) =
      str -> summon[BsonWriter[A]].write(value)

case object BsonNull extends BsonValue:
  override def asJava: JavaBsonNull = JavaBsonNull.VALUE

case class BsonBoolean(value: Boolean) extends BsonValue:
  override def asJava: JavaBsonBoolean =
    if (value) JavaBsonBoolean.TRUE else JavaBsonBoolean.FALSE

object BsonBoolean:
  val TRUE: BsonBoolean = BsonBoolean(true)
  val FALSE: BsonBoolean = BsonBoolean(false)

case class BsonString(value: String) extends BsonValue:
  override def asJava: JavaBsonString = new JavaBsonString(value)

case class BsonInt32(value: Int) extends BsonValue:
  override def asJava: JavaBsonInt32 = new JavaBsonInt32(value)

case class BsonInt64(value: Long) extends BsonValue:
  override def asJava: JavaBsonInt64 = new JavaBsonInt64(value)

case class BsonDecimal128(value: Decimal128) extends BsonValue:
  override def asJava: JavaBsonDecimal128 = new JavaBsonDecimal128(value)

case class BsonDouble(value: Double) extends BsonValue:
  override def asJava: JavaBsonDouble = new JavaBsonDouble(value)

case class BsonDateTime(value: Instant) extends BsonValue:
  override def asJava: JavaBsonDateTime = new JavaBsonDateTime(
    value.toEpochMilli
  )

case class BsonObjectId(value: ObjectId) extends BsonValue:
  override def asJava: JavaBsonObjectId = new JavaBsonObjectId(value)

object BsonValue:
  private def convertJavaBsonDocument(d: JavaBsonDocument): BsonDocument =
    val fields =
      CollectionConverters
        .asScala(d.entrySet())
        .map: entry =>
          entry.getKey -> apply(entry.getValue)
    BsonDocument(fields.toMap)

  def apply(value: JavaBsonValue): BsonValue =
    value match
      case _: JavaBsonNull    => BsonNull
      case b: JavaBsonBoolean => BsonBoolean(b.getValue)
      case a: JavaBsonArray =>
        val values: Seq[JavaBsonValue] =
          CollectionConverters.asScala(a.getValues).toSeq
        BsonArray(values.map(apply))
      case d: JavaBsonDocument   => convertJavaBsonDocument(d)
      case s: JavaBsonString     => BsonString(s.getValue)
      case i: JavaBsonInt32      => BsonInt32(i.getValue)
      case l: JavaBsonInt64      => BsonInt64(l.getValue)
      case d: JavaBsonDecimal128 => BsonDecimal128(d.getValue)
      case d: JavaBsonDouble     => BsonDouble(d.getValue)
      case dt: JavaBsonDateTime =>
        BsonDateTime(Instant.ofEpochMilli(dt.getValue))
      case id: JavaBsonObjectId =>
        BsonObjectId(id.getValue)
      case v =>
        throw new UnsupportedOperationException(
          s"Types of ${v.getClass.getName} are not supported"
        )
  val codec: codecs.Codec[BsonValue] = new Codec[BsonValue]:
    val delegate = new BsonValueCodec()
    override def decode(
        reader: bson.BsonReader,
        decoderContext: DecoderContext
    ): BsonValue =
      BsonValue(delegate.decode(reader, decoderContext))

    override def encode(
        writer: bson.BsonWriter,
        value: BsonValue,
        encoderContext: EncoderContext
    ): Unit =
      delegate.encode(writer, value.asJava, encoderContext)

    override def getEncoderClass: Class[BsonValue] = classOf[BsonValue]

  val documentCodec: codecs.Codec[BsonDocument] = new Codec[BsonDocument]:
    val delegate = new codecs.BsonDocumentCodec()

    override def decode(
        reader: bson.BsonReader,
        decoderContext: DecoderContext
    ): BsonDocument =
      convertJavaBsonDocument(delegate.decode(reader, decoderContext))

    override def encode(
        writer: bson.BsonWriter,
        value: BsonDocument,
        encoderContext: EncoderContext
    ): Unit =
      delegate.encode(writer, value.asJava, encoderContext)

    override def getEncoderClass: Class[BsonDocument] = classOf[BsonDocument]

  val codecRegistry: CodecRegistry = CodecRegistries.fromRegistries(
    CodecRegistries.fromCodecs(codec, documentCodec),
    MongoClientSettings.getDefaultCodecRegistry
  );
