package uk.co.unclealex.mongodb.dao

import com.mongodb.MongoClientSettings
import com.mongodb.reactivestreams.client.{
  FindPublisher,
  MongoCollection,
  MongoDatabase
}
import org.apache.pekko.NotUsed
import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.{Keep, Sink, Source}
import org.bson
import org.bson.codecs
import org.bson.codecs.configuration.{CodecRegistries, CodecRegistry}
import org.bson.conversions.Bson
import uk.co.unclealex.mongodb.bson.*
import uk.co.unclealex.mongodb.bson.BsonDocument.~>
import uk.co.unclealex.mongodb.dao.Database.Database
import uk.co.unclealex.mongodb.persistable.{
  HasDateCreated,
  HasLastUpdated,
  Persistable,
  Upsertable
}

import java.time.{Clock, Instant}
import scala.concurrent.{ExecutionContext, Future}
import scala.reflect.ClassTag

/** A base class for MongoDB DAOs.
  *
  * @param database
  *   The underlying Mongo database.
  * @param collectionName
  *   The name of the collection queried and mutated by this DAO.
  */
abstract class MongoDbDao[V: ClassTag](
    private val eventualDatabase: Future[Database],
    private val clock: Clock,
    private val collectionName: String
)(using
    ec: ExecutionContext,
    actorSystem: ActorSystem,
    persistable: Persistable[V],
    hasDateCreated: HasDateCreated[V] = HasDateCreated.empty[V],
    hasLastUpdated: HasLastUpdated[V] = HasLastUpdated.empty[V]
) extends MongoDSL[V]:

  val dsl: MongoDSL[V] = this

  val indicies: Seq[Index[V]] = Seq.empty

  val defaultSort: Option[Sort] = None

  private type SourceV = Source[V, NotUsed]
  private type SourceLong = Source[Long, NotUsed]

  private val ready: Future[MongoDatabase] =
    def createIndicies(
        collection: MongoCollection[BsonDocument]
    ): Source[String, NotUsed] =
      Source(indicies).flatMap: index =>
        Source.fromPublisher:
          collection.createIndex(index.asBson, index.asOptions)
    for
      db <- eventualDatabase
      coll = db
        .getCollection[BsonDocument](collectionName, classOf[BsonDocument])
      _ <- createIndicies(coll).runWith(Sink.seq)
    yield db

  val documentCodec: codecs.Codec[V] = new codecs.Codec[V]:

    override def decode(
        reader: bson.BsonReader,
        decoderContext: codecs.DecoderContext
    ): V =
      summon[Persistable[V]]
        .readDocument(
          BsonValue.documentCodec.decode(reader, decoderContext)
        )
        .get

    override def encode(
        writer: bson.BsonWriter,
        value: V,
        encoderContext: codecs.EncoderContext
    ): Unit =
      BsonValue.documentCodec.encode(
        writer,
        summon[Persistable[V]].writeDocument(value),
        encoderContext
      )

    override def getEncoderClass: Class[V] =
      summon[ClassTag[V]].runtimeClass.asInstanceOf[Class[V]]

  val codecRegistry: CodecRegistry = CodecRegistries.fromRegistries(
    CodecRegistries.fromCodecs(documentCodec),
    MongoClientSettings.getDefaultCodecRegistry
  );

  /** Eventually get the collection.
    *
    * @return
    */
  def collection(): Source[MongoCollection[V], NotUsed] =
    Source.future:
      ready.map: db =>
        db.getCollection[V](
          collectionName,
          summon[ClassTag[V]].runtimeClass.asInstanceOf[Class[V]]
        ).withCodecRegistry(BsonValue.codecRegistry)
          .withCodecRegistry(codecRegistry)

  def bsonCollection(): Source[MongoCollection[BsonDocument], NotUsed] =
    Source.future:
      ready.map: db =>
        db.getCollection[BsonDocument](
          collectionName,
          classOf[BsonDocument]
        ).withCodecRegistry(BsonValue.codecRegistry)

  private def insert(v: V, now: Instant): SourceV =
    val valueToInsert = updateId(ID())
      .compose(updateLastUpdated(now))
      .compose(updateDateCreated(now))(v)
    for
      collection <- collection()
      _ <- Source.fromPublisher(collection.insertOne(valueToInsert))
    yield valueToInsert

  private def replace(v: V, now: Instant): SourceV =
    val replacement: V = updateLastUpdated(now)(v)
    val id = v._id.get
    for
      collection <- collection()
      _ <- Source.fromPublisher(collection.replaceOne(idQuery(id), replacement))
    yield replacement

  def store(v: V): SourceV =
    val now: Instant = clock.instant()
    v._id match
      case Some(_) => replace(v, now)
      case None    => insert(v, now)

  private def updateId(id: ID)(value: V): V =
    value._id match
      case Some(_) => value
      case _       => value.withId(id)

  private def updateDateCreated(dateCreated: Instant)(value: V): V =
    value.dateCreated match
      case Some(_) => value
      case _       => value.setDateCreated(dateCreated)

  private def updateLastUpdated(lastUpdated: Instant)(value: V): V =
    value.updateLastUpdated(lastUpdated)

  def update(
      filter: Query,
      changes: V => V,
      sort: Option[Sort] = None
  ): SourceV = {
    update(filter, changes, sort, clock.instant())
  }

  private def update(
      filter: Query,
      changes: V => V,
      sort: Option[Sort],
      now: Instant
  ): SourceV = {
    val allChanges: V => V = changes.compose(updateLastUpdated(now))
    findWhere(filter, sort).flatMapConcat { value =>
      val newValue = allChanges(value)
      store(newValue)
    }
  }

  def upsert(filter: Query, changes: V => V, sort: Option[Sort] = None)(using
      Upsertable[V]
  ): SourceV =
    val now: Instant = clock.instant()
    val zero: V = summon[Upsertable[V]].zero
    val insertSource: Source[V, Future[NotUsed]] = Source.lazySource { () =>
      insert(changes(zero), now)
    }
    update(filter, changes, sort, now).orElseMat(insertSource)(Keep.left)

  /** Find a document by its ObjectId.
    */
  def findById(id: ID): SourceV = {
    findOne(idQuery(id))
  }

  private def idQuery(id: ID): Query =
    Query(BsonDocument("_id" -> BsonDocument("$eq" ~> id)))

  private def resolveSort(sort: Option[Sort]) =
    sort.orElse(defaultSort)

  def findOne(filter: Query, sort: Option[Sort] = None): SourceV = {
    findWhere(filter, sort, 1)
  }

  def findWhere(
      filter: Query,
      sort: Option[Sort] = None,
      maxValues: Int = Int.MaxValue
  ): SourceV =
    def maybeSorted(findPublisher: FindPublisher[V]): FindPublisher[V] =
      resolveSort(sort)
        .map(summon[Conversion[Sort, Bson]])
        .foldLeft(findPublisher)(_.sort(_))
    val source: SourceV = for
      collection <- collection()
      value <- Source.fromPublisher(maybeSorted(collection.find(filter)))
    yield value
    source.take(maxValues)

  def findAll(
      sort: Option[Sort] = None,
      maxValues: Int = Integer.MAX_VALUE
  ): SourceV = {
    findWhere(Query.all, sort, maxValues)
  }

  def removeWhere(filter: Query): Source[Long, NotUsed] =
    for
      collection <- collection()
      result <- Source.fromPublisher(collection.deleteMany(filter))
    yield result.getDeletedCount

  def removeOne(filter: Query): Source[Long, NotUsed] =
    for
      collection <- collection()
      result <- Source.fromPublisher(collection.deleteOne(filter))
    yield result.getDeletedCount

  def removeById(id: ID): Source[Long, NotUsed] = removeOne(idQuery(id))
