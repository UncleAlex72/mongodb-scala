package uk.co.unclealex.mongodb.dao

import org.bson.conversions.Bson
import uk.co.unclealex.mongodb.bson.BsonDocument.~>
import uk.co.unclealex.mongodb.bson.{
  BsonArray,
  BsonDocument,
  BsonValue,
  BsonWriter
}
import uk.co.unclealex.mongodb.macros.FieldExists

import scala.annotation.targetName

case class Query(private val bsonDocument: BsonDocument = BsonDocument()):

  @targetName("and")
  def &&(other: Query): Query = Query.and(this, other)

  @targetName("or")
  def ||(other: Query): Query = Query.or(this, other)

object Query:
  val all: Query = Query()

  inline def compareBson[A](
      field: String,
      comparator: String,
      value: BsonValue
  ): Query =
    Query(
      BsonDocument(FieldExists[A](field) -> BsonDocument(comparator -> value))
    )

  inline def compare[A, V](field: String, comparator: String, value: V)(using
      BsonWriter[V]
  ): Query =
    Query(
      BsonDocument(FieldExists[A](field) -> BsonDocument(comparator ~> value))
    )

  private def combine(combinator: String, queries: Seq[Query]): Query =
    val queryArray: BsonArray = BsonArray(queries.map(_.bsonDocument))
    val document = BsonDocument(combinator -> queryArray)
    new Query(document)

  def and(queries: Query*): Query = combine("$and", queries)

  def or(queries: Query*): Query = combine("$or", queries)

  given Conversion[Query, Bson] = _.bsonDocument.asJava
