package uk.co.unclealex.mongodb.dao

import uk.co.unclealex.mongodb.bson.{BsonDocument, BsonValue, BsonWriter}

import scala.annotation.targetName

class Update(private val bsonDocument: BsonDocument):

  @targetName("combine")
  def &&(otherUpdate: Update): Update = new Update(
    bsonDocument.merge(otherUpdate.bsonDocument)
  )

object Update:

  def $set(doc: BsonDocument): Update =
    new Update(BsonDocument("$set" -> doc))

  def $set(fieldName: String, bsonValue: BsonValue): Update =
    new Update(BsonDocument("$set" -> BsonDocument(fieldName -> bsonValue)))

  def $set[A](using BsonWriter[A])(fieldName: String, value: A): Update =
    $set(fieldName, summon[BsonWriter[A]].write(value))
