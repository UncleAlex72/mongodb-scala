package uk.co.unclealex.mongodb.bson

import org.bson.types.Decimal128

import uk.co.unclealex.stringlike.StringLike

import java.time.Instant
import scala.reflect.ClassTag
import scala.util.{Success, Try}

trait BsonReader[A]:

  def read(value: BsonValue): Try[A]

trait BsonReaderInstances:

  def readPF[A: ClassTag](pf: PartialFunction[BsonValue, A]): BsonReader[A] =
    (value: BsonValue) =>
      pf
        .lift(value)
        .toRight:
          val className = summon[ClassTag[A]].getClass.getName
          new IllegalArgumentException(
            s"$className cannot be read from type ${value.getClass.getName}"
          )
        .toTry

  def readPFT[A: ClassTag](
      pf: PartialFunction[BsonValue, Try[A]]
  ): BsonReader[A] =
    (value: BsonValue) => readPF(pf).read(value).flatten

  given [A](using BsonReader[A]): BsonReader[Seq[A]] =
    readPFT:
      case ba: BsonArray =>
        Try:
          ba.values.map: v =>
            summon[BsonReader[A]].read(v).get

  given [A](using BsonReader[A]): BsonReader[Option[A]] =
    readPFT:
      case BsonNull      => Success(None)
      case bv: BsonValue => summon[BsonReader[A]].read(bv).map(Some(_))

  given [A: ClassTag](using StringLike[A]): BsonReader[A] =
    readPFT:
      case bs: BsonString => summon[StringLike[A]].tryDecode(bs.value)

  given BsonReader[String] =
    readPF:
      case bs: BsonString => bs.value

  given BsonReader[Int] =
    readPF:
      case bi: BsonInt32 => bi.value

  given BsonReader[Long] =
    readPF:
      case bl: BsonInt64 => bl.value

  given BsonReader[Decimal128] =
    readPF:
      case bd: BsonDecimal128 => bd.value

  given BsonReader[Double] =
    readPF:
      case bd: BsonDouble => bd.value

  given BsonReader[Boolean] =
    readPF:
      case bb: BsonBoolean => bb.value

  given BsonReader[ID] =
    readPF:
      case bid: BsonObjectId => ID(bid.value.toHexString)

  given BsonReader[Instant] =
    readPF:
      case bdt: BsonDateTime => bdt.value

given give_bson_document_reader_bson_reader[A](using
    BsonDocumentReader[A]
): BsonReader[A] with
  override def read(b: BsonValue): Try[A] =
    b match
      case bd: BsonDocument => summon[BsonDocumentReader[A]].readDocument(bd)
      case _ => throw new IllegalStateException("A document is required")

object BsonReader extends BsonReaderInstances
