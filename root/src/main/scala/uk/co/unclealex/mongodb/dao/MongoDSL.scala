package uk.co.unclealex.mongodb.dao

trait MongoDSL[A]
    extends QueryDSL[A]
    with UpdateDSL[A]
    with SortDSL[A]
    with IndexDsl[A]
