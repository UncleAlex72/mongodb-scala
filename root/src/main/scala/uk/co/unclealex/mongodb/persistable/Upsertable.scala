package uk.co.unclealex.mongodb.persistable

/** A typeclass that allows [[MongoDbDao]] to run upsert queries, where a 'zero'
  * object is created on an insert.
  */
trait Upsertable[V] {

  def zero: V
}

object Upsertable {
  def apply[V](v: V): Upsertable[V] = new Upsertable[V] {
    override def zero: V = v
  }
}
