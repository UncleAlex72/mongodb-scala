package uk.co.unclealex.mongodb.bson

import scala.util.Try

trait BsonCodec[A] extends BsonReader[A] with BsonWriter[A]
