package uk.co.unclealex.mongodb.persistable

import uk.co.unclealex.mongodb.macros.ChangeValue

import java.time.Instant

trait HasDateCreated[A]:

  extension (a: A)
    def dateCreated: Option[Instant]
    def setDateCreated(newDateCreated: Instant): A

object HasDateCreated:
  class HasDateCreatedFromFunctions[A](
      getter: A => Option[Instant],
      setter: A => Instant => A
  ) extends HasDateCreated[A]:
    extension (a: A)
      override def dateCreated: Option[Instant] = getter(a)
      override def setDateCreated(newDateCreated: Instant): A =
        setter(a)(newDateCreated)

  inline def derived[A <: { val dateCreated: Option[Instant] }]
      : HasDateCreated[A] =
    import scala.reflect.Selectable.reflectiveSelectable
    def getter(a: A): Option[Instant] = a.dateCreated
    def setter(a: A)(newDateCreated: Instant): A =
      a.dateCreated match
        case Some(_) =>
          a
        case None =>
          ChangeValue(a, "dateCreated", Some(newDateCreated))
    new HasDateCreatedFromFunctions[A](getter, setter)

  def empty[A]: HasDateCreated[A] =
    new HasDateCreatedFromFunctions[A](_ => None, a => inst => a)
