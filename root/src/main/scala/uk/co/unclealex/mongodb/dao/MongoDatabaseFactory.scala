package uk.co.unclealex.mongodb.dao

import com.mongodb.reactivestreams.client.{MongoClients, MongoDatabase}
import com.mongodb.{
  ConnectionString,
  MongoClientSettings,
  ServerApi,
  ServerApiVersion
}
import uk.co.unclealex.mongodb.dao.Database.Database

import scala.concurrent.{ExecutionContext, Future}
object MongoDatabaseFactory:

  def apply(url: String)(using
      ec: ExecutionContext
  ): (Future[AutoCloseable], Future[Database]) =
    val finalSlash = url.lastIndexOf('/')
    if (finalSlash < 0)
      throw new IllegalArgumentException(
        s"$url is not a valid mongodb connection url"
      )
    val dbUrl = new ConnectionString(url.substring(0, finalSlash))
    val dbName = url.substring(finalSlash + 1)
    val serverApi = ServerApi.builder.version(ServerApiVersion.V1).build
    val settings = MongoClientSettings.builder
      .applyConnectionString(dbUrl)
      .serverApi(serverApi)
      .build
    val mongoClient = MongoClients.create(settings)
    val mongoDatabase = mongoClient.getDatabase(dbName)
    (Future.successful(mongoClient), Future.successful(mongoDatabase))
