package uk.co.unclealex.mongodb.bson

import shapeless3.deriving.K0

import scala.util.Try

trait BsonDocumentCodec[A]
    extends BsonDocumentReader[A]
    with BsonDocumentWriter[A]
    with BsonCodec[A]

trait BsonDocumentCodecInstances:

  class DelegatingBsonDocumentCodec[A](
      reader: BsonDocumentReader[A],
      writer: BsonDocumentWriter[A]
  ) extends BsonDocumentCodec[A]:
    override def readDocument(doc: BsonDocument): Try[A] =
      reader.readDocument(doc)
    override def writeDocument(a: A): BsonDocument = writer.writeDocument(a)

  given [A](using
      BsonDocumentReader[A],
      BsonDocumentWriter[A]
  ): BsonDocumentCodec[A] =
    new DelegatingBsonDocumentCodec[A](
      summon[BsonDocumentReader[A]],
      summon[BsonDocumentWriter[A]]
    )

  inline def derived[A](using gen: K0.Generic[A]): BsonDocumentCodec[A] =
    new DelegatingBsonDocumentCodec[A](
      BsonDocumentReader.derived[A],
      BsonDocumentWriter.derived[A]
    )

object BsonDocumentCodec extends BsonDocumentCodecInstances
