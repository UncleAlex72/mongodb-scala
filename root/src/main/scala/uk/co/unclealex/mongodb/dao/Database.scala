package uk.co.unclealex.mongodb.dao

import com.mongodb.reactivestreams.client.MongoDatabase

object Database:
  type Database = MongoDatabase
