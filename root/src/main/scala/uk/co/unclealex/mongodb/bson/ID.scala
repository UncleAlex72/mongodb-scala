package uk.co.unclealex.mongodb.bson

import io.circe.Codec
import org.bson.types.ObjectId
import uk.co.unclealex.stringlike.{StringLike, StringLikeJsonSupport}

/** A small class used to model mongo DB IDs. The 12 byte ID is mapped to its
  * lower case hexadecimal representation.
  *
  * @param id
  *   The ID of the object.
  */
case class ID(id: String)

object ID {

  def apply(): ID =
    val hexString = new ObjectId().toHexString
    ID(hexString)

  given StringLike[ID] = StringLike.fromFunctions(_.id, ID(_))

  given Codec[ID] = StringLikeJsonSupport.circeCodec
}
