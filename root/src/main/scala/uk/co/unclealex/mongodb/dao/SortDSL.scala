package uk.co.unclealex.mongodb.dao

import uk.co.unclealex.mongodb.macros.FieldExists

trait SortDSL[A]:

  extension (field: String)

    private inline def sort(sortType: SortType): Option[Sort] = Some:
      Sort(Map(FieldExists[A](field) -> sortType))
    inline def asc: Option[Sort] = sort(SortType.Ascending)
    inline def desc: Option[Sort] = sort(SortType.Descending)
