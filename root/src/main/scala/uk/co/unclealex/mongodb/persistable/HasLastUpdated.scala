package uk.co.unclealex.mongodb.persistable

import uk.co.unclealex.mongodb.macros.ChangeValue

import java.time.Instant

trait HasLastUpdated[A]:

  def fieldName: Option[String]

  extension (a: A)
    def lastUpdated: Option[Instant]
    def updateLastUpdated(newLastUpdated: Instant): A

object HasLastUpdated:
  class HasLastUpdatedFromFunctions[A](
      maybeFieldName: Option[String],
      getter: A => Option[Instant],
      setter: A => Instant => A
  ) extends HasLastUpdated[A]:

    def fieldName: Option[String] = maybeFieldName
    extension (a: A)
      override def lastUpdated: Option[Instant] = getter(a)
      override def updateLastUpdated(newLastUpdated: Instant): A =
        setter(a)(newLastUpdated)

  inline def derived[A <: { val lastUpdated: Option[Instant] }]
      : HasLastUpdated[A] =
    import scala.reflect.Selectable.reflectiveSelectable
    new HasLastUpdatedFromFunctions[A](
      Some("lastUpdated"),
      _.lastUpdated,
      a => newLastUpdated => ChangeValue(a, "lastUpdated", Some(newLastUpdated))
    )

  def empty[A]: HasLastUpdated[A] =
    new HasLastUpdatedFromFunctions[A](None, _ => None, a => _ => a)
