package uk.co.unclealex.mongodb.bson

import shapeless3.deriving.*
import uk.co.unclealex.mongodb.*

import scala.util.{Failure, Success, Try}

trait BsonDocumentReader[A] extends BsonReader[A]:

  def readDocument(doc: BsonDocument): Try[A]

  override def read(value: BsonValue): Try[A] = value match
    case b: BsonDocument => readDocument(b)
    case _ => Failure(new IllegalStateException("Expected a bson document"))

object BsonDocumentReader:

  private case class Acc(fields: List[BsonValue], maybeError: Option[Throwable])
  private def deriveBsonReaderProduct[T](using
      inst: K0.ProductInstances[BsonReader, T],
      labelling: Labelling[T]
  ): BsonDocumentReader[T] = (doc: BsonDocument) =>
    val fields = doc.fields
    val orderedFields: List[BsonValue] = labelling.elemLabels.toList.map:
      child => fields.getOrElse(child, BsonNull)

    inst.unfold[Acc](Acc(fields = orderedFields, maybeError = None))(
      [t] =>
        (acc: Acc, reader: BsonReader[t]) =>
          acc.fields match
            case head :: tail =>
              reader.read(head) match
                case Success(t) =>
                  (Acc(fields = tail, maybeError = None), Some(t))
                case Failure(e) =>
                  (Acc(fields = Nil, maybeError = Some(e)), None)
            case Nil => (acc, None)
    ) match
      case (Acc(_, Some(e)), None) => Failure(e)
      case (Acc(_, None), None) =>
        Failure(
          new IllegalStateException(s"value is not valid ${labelling.label}")
        )
      case (_, Some(t)) => Success(t)

  private def deriveBsonReaderSum[A](using
      cInst: K0.CoproductInstances[BsonReader, A],
      labelling: Labelling[A]
  ): BsonDocumentReader[A] = (doc: BsonDocument) =>
    doc.get("className") match
      case None =>
        Failure:
          new IllegalStateException("Cannot find a discriminator field")
      case Some(BsonString(className)) =>
        val selectedIndex = labelling.elemLabels.indexOf(className)
        if (selectedIndex < 0)
          Failure:
            new IllegalStateException(
              s"$className is not a valid ${labelling.label}"
            )
        else
          cInst.inject(selectedIndex)(
            [t <: A] => (reader: BsonReader[t]) => reader.read(doc)
          )
      case _ =>
        Failure:
          new IllegalStateException(s"A discriminator field must be a string")

  inline def derived[A](using gen: K0.Generic[A]): BsonDocumentReader[A] =
    gen.derive(deriveBsonReaderProduct, deriveBsonReaderSum)

  given BsonDocumentReader[BsonDocument] with
    override def readDocument(doc: BsonDocument): Try[BsonDocument] = Success(
      doc
    )
