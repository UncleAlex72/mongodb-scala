package uk.co.unclealex.mongodb.dao

import com.mongodb.client.model.{IndexOptions, Indexes}
import org.bson.conversions.Bson
import IndexType.{Ascending, Descending}
import uk.co.unclealex.mongodb.macros.FieldExists

import scala.jdk.javaapi.CollectionConverters.asJava

case class Index[A](
    name: Option[String] = None,
    _unique: Boolean = false,
    columns: Seq[String] = Seq.empty,
    indexType: IndexType = IndexType.Ascending
):

  inline def on(column: String): Index[A] =
    copy(columns = columns :+ FieldExists[A](column))

  def named(name: String): Index[A] = copy(name = Some(name))

  def unique: Index[A] = copy(_unique = true)

  def ascending: Index[A] = copy(indexType = Ascending)
  def descending: Index[A] = copy(indexType = Descending)

  def asBson: Bson =
    val fn: java.util.List[String] => Bson = indexType match
      case Ascending  => Indexes.ascending
      case Descending => Indexes.descending
    fn(asJava(columns))

  def asOptions: IndexOptions =
    name.foldLeft(new IndexOptions().unique(_unique)): (options, name) =>
      options.name(name)

enum IndexType:
  case Ascending
  case Descending
