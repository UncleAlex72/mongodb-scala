package uk.co.unclealex.mongodb.bson

import org.bson.types.{Decimal128, ObjectId}
import uk.co.unclealex.mongodb.*
import uk.co.unclealex.stringlike.StringLike

import java.time.Instant

trait BsonWriter[A]:

  def write(a: A): BsonValue

trait BsonWriterInstances:

  given [A](using BsonWriter[A]): BsonWriter[Seq[A]] with
    override def write(as: Seq[A]): BsonValue =
      val array = as.map(a => summon[BsonWriter[A]].write(a))
      BsonArray(array)

  given [B <: BsonValue]: BsonWriter[Map[String, B]] with
    override def write(as: Map[String, B]): BsonValue =
      BsonDocument(as)

  given [A](using BsonWriter[A]): BsonWriter[Option[A]] with
    override def write(oa: Option[A]): BsonValue =
      oa match
        case Some(a) => summon[BsonWriter[A]].write(a)
        case _       => BsonNull

  given [A](using StringLike[A]): BsonWriter[A] with
    override def write(a: A): BsonValue =
      BsonString(summon[StringLike[A]].encode(a))

  given BsonWriter[String] with
    override def write(str: String): BsonValue =
      BsonString(str)

  given BsonWriter[Int] with
    override def write(i: Int): BsonValue = BsonInt32(i)

  given BsonWriter[Long] with
    override def write(l: Long): BsonValue = BsonInt64(l)

  given BsonWriter[Decimal128] with
    override def write(d: Decimal128): BsonValue = BsonDecimal128(d)

  given BsonWriter[Double] with
    override def write(d: Double): BsonValue = BsonDouble(d)

  given BsonWriter[Boolean] with
    override def write(b: Boolean): BsonValue = BsonBoolean(b)

  given BsonWriter[ID] with
    override def write(id: ID): BsonValue = new BsonObjectId(
      new ObjectId(id.id)
    )

  given BsonWriter[Instant] with
    override def write(i: Instant): BsonValue = BsonDateTime(i)

  given give_bson_document_writer_bson_writer[A](using
      BsonDocumentWriter[A]
  ): BsonWriter[A] with
    override def write(a: A): BsonValue =
      summon[BsonDocumentWriter[A]].writeDocument(a)

object BsonWriter extends BsonWriterInstances
