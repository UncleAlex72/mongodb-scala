package uk.co.unclealex.mongodb.test

import com.mongodb.reactivestreams.client.MongoDatabase
import com.typesafe.scalalogging.StrictLogging
import enumeratum.*
import uk.co.unclealex.mongodb.bson.{BsonDocument, BsonInt32, BsonNull}
import uk.co.unclealex.mongodb.dao.{Query, QueryDSL}

import java.time.Clock
import scala.collection.immutable
import scala.concurrent.Future

class MongoDbDaoQuerySpec
    extends MongoDbDaoBaseSpec[Num, NumMongoDbDao]("num")
    with StrictLogging:

  "streaming all elements" should {
    "stream all elements in order" in: f =>
      for objs <- f.dao.findAll().many
      yield objs.map(_.num) should contain theSameElementsInOrderAs Seq(
        None,
        Some(1),
        Some(2),
        Some(3),
        Some(4),
        Some(5),
        Some(6),
        Some(7),
        Some(8),
        Some(9),
        Some(10)
      )

    "stream all elements sorted by a given ordering" in: f =>
      import f.dao.*
      for objs <- f.dao.findAll(sort = "num".desc).many
      yield objs.map(_.num) should contain theSameElementsInOrderAs Seq(
        Some(10),
        Some(9),
        Some(8),
        Some(7),
        Some(6),
        Some(5),
        Some(4),
        Some(3),
        Some(2),
        Some(1),
        None
      )

    "not stream more than the maximum number of elements requested" in: f =>
      for objs <- f.dao.findAll(maxValues = 5).many
      yield objs.map(_.num) should contain theSameElementsInOrderAs Seq(
        None,
        Some(1),
        Some(2),
        Some(3),
        Some(4)
      )

  }

  "streaming all elements with null values" should {
    "stream only the elements with null values" in: f =>
      import f.dao.*
      for objs <- f.dao.findWhere("num".isNull).many
      yield objs
        .map(_.num) should contain theSameElementsInOrderAs Seq(None)

  }

  "streaming elements with an or query" should {
    "return all the elements that match the queries" in: f =>
      import f.dao.*
      for objs <- f.dao.findWhere("num" === 3 || "num" === 4).many
      yield objs
        .map(_.num) should contain theSameElementsInOrderAs Seq(
        Some(3),
        Some(4)
      )

  }

  "streaming elements with an and query" should {
    "return only the elements that match all the queries" in: f =>
      import f.dao.*
      for objs <- f.dao.findWhere("num" > 3 && "num" < 6).many
      yield objs
        .map(_.num) should contain theSameElementsInOrderAs Seq(
        Some(4),
        Some(5)
      )

  }
  "streaming all elements less than a specific value" should {
    "stream only smaller elements" in: f =>
      import f.dao.*
      for objs <- f.dao.findWhere("num" < 5).many
      yield objs
        .map(_.num) should contain theSameElementsInOrderAs Seq(
        Some(1),
        Some(2),
        Some(3),
        Some(4)
      )

  }

  "streaming all elements less than or equal to a specific value" should {
    "stream only smaller and equal elements" in: f =>
      import f.dao.*
      for objs <- f.dao.findWhere("num" <= 5).many
      yield objs.map(_.num) should contain theSameElementsInOrderAs Seq(
        Some(1),
        Some(2),
        Some(3),
        Some(4),
        Some(5)
      )

  }

  "streaming all elements greater than a specific value" should {
    "stream only larger elements" in: f =>
      import f.dao.*
      for objs <- f.dao.findWhere("num" > 5).many
      yield objs.map(_.num) should contain theSameElementsInOrderAs Seq(
        Some(6),
        Some(7),
        Some(8),
        Some(9),
        Some(10)
      )

  }

  "streaming all elements greater than or equal to a specific value" should {
    "stream only larger or equal elements" in: f =>
      import f.dao.dsl.*
      for objs <- f.dao.findWhere("num" >= 5).many
      yield objs.map(_.num) should contain theSameElementsInOrderAs Seq(
        Some(5),
        Some(6),
        Some(7),
        Some(8),
        Some(9),
        Some(10)
      )

  }

  enum Bound(val name: String):
    case Inclusive extends Bound("inclusive")
    case Exclusive extends Bound("exclusive")

  sealed trait SearchQuery(
      val lowerBound: Bound,
      val upperBound: Bound,
      val query: (Option[Int], Option[Int]) => Query,
      val someNone: Seq[Int],
      val noneSome: Seq[Int],
      val someSome: Seq[Int]
  ) extends EnumEntry

  object SearchQuery extends Enum[SearchQuery] with QueryDSL[Num]:

    override def values: IndexedSeq[SearchQuery] = findValues

    case object LowerInclusiveUpperInclusive
        extends SearchQuery(
          lowerBound = Bound.Inclusive,
          upperBound = Bound.Inclusive,
          query = "num".?>=&<=[Int],
          someNone = Seq(3, 4, 5, 6, 7, 8, 9, 10),
          noneSome = Seq(1, 2, 3, 4, 5, 6, 7),
          someSome = Seq(3, 4, 5, 6, 7)
        )

    case object LowerInclusiveUpperExclusive
        extends SearchQuery(
          lowerBound = Bound.Inclusive,
          upperBound = Bound.Exclusive,
          query = "num".?>=&<[Int],
          someNone = Seq(3, 4, 5, 6, 7, 8, 9, 10),
          noneSome = Seq(1, 2, 3, 4, 5, 6),
          someSome = Seq(3, 4, 5, 6)
        )

    case object LowerExclusiveUpperInclusive
        extends SearchQuery(
          lowerBound = Bound.Exclusive,
          upperBound = Bound.Inclusive,
          query = "num".?>&<=[Int],
          someNone = Seq(4, 5, 6, 7, 8, 9, 10),
          noneSome = Seq(1, 2, 3, 4, 5, 6, 7),
          someSome = Seq(4, 5, 6, 7)
        )

    case object LowerExclusiveUpperExclusive
        extends SearchQuery(
          lowerBound = Bound.Exclusive,
          upperBound = Bound.Exclusive,
          query = "num".?>&<[Int],
          someNone = Seq(4, 5, 6, 7, 8, 9, 10),
          noneSome = Seq(1, 2, 3, 4, 5, 6),
          someSome = Seq(4, 5, 6)
        )

  SearchQuery.values.foreach: searchQuery =>
    s"The search query with an ${searchQuery.lowerBound.name} lower bound and a ${searchQuery.upperBound.name} upper bound" should {
      "find everything when there are no bounds" in: f =>
        val query: Query = searchQuery.query(None, None)
        for results <- f.dao.findWhere(query).many
        yield results.flatMap(
          _.num
        ) should contain theSameElementsInOrderAs Seq(1, 2, 3, 4, 5, 6, 7, 8, 9,
          10)

      "find larger elements when there is a lower bound" in: f =>
        val query: Query = searchQuery.query(Some(3), None)
        for results <- f.dao.findWhere(query).many
        yield results.flatMap(
          _.num
        ) should contain theSameElementsInOrderAs searchQuery.someNone

      "find smaller elements when there is an upper bound" in: f =>
        val query: Query = searchQuery.query(None, Some(7))
        for results <- f.dao.findWhere(query).many
        yield results.flatMap(
          _.num
        ) should contain theSameElementsInOrderAs searchQuery.noneSome

      "find in between elements when there is a lower and an upper bound" in:
        f =>
          val query: Query = searchQuery.query(Some(3), Some(7))
          for results <- f.dao.findWhere(query).many
          yield results.flatMap(
            _.num
          ) should contain theSameElementsInOrderAs searchQuery.someSome
    }

  override def createDao(
      eventualDatabase: Future[MongoDatabase],
      clock: Clock
  ) =
    new NumMongoDbDao(eventualDatabase, clock)

  override def initialData(): Seq[BsonDocument] =
    Seq(1, 5, 10, 3, 2, 9, 8, 6, 7, 4).map(num =>
      BsonDocument("num" -> BsonInt32(num))
    ) :+ BsonDocument("num" -> BsonNull)
