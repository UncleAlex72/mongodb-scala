package uk.co.unclealex.mongodb.test

import com.mongodb.reactivestreams.client.MongoDatabase
import uk.co.unclealex.days.Syntax.*
import uk.co.unclealex.days.ZonedDSL
import org.bson.BsonDocument as JavaBsonDocument
import uk.co.unclealex.mongodb.bson.{BsonDocument, BsonInt32, BsonString, ID}
import uk.co.unclealex.mongodb.dao.Query

import java.time.{Clock, Instant, ZoneId}
import scala.concurrent.Future

class MongoDbDaoSpec
    extends MongoDbDaoBaseSpec[Musician, MusicianMongoDbDao]("musician")
    with ZonedDSL:

  given zoneId: ZoneId = ZoneId.of("Europe/London")

  val freddie: Musician =
    Musician("Freddie", "Mercury", September(5, 1946) at 1.am)
  val brian: Musician = Musician("Brian", "May", July(19, 1947) at 3.pm)
  val roger: Musician = Musician("Roger", "Taylor", July(26, 1949) at 3.am)
  val john: Musician = Musician("John", "Deacon", August(19, 1951) at 4.pm)

  val invalidId: ID = ID("000000000000000000000000")

  val (farrokh, bulsara) = ("Farrokh", "Bulsara")

  "The DAO" should {
    "create any indexes specified" in: f =>
      for
        coll <- f.bsonCollection
        indexes <- coll.listIndexes(classOf[JavaBsonDocument]).many
      yield indexes should contain(
        BsonDocument(
          "v" -> BsonInt32(2),
          "key" -> BsonDocument("surname" -> BsonInt32(1)),
          "name" -> BsonString("index_name")
        ).asJava
      )
  }

  "Inserting a new element" should {
    "populate its ID, last updated and creation date" in: f =>
      val now: Instant = Instant.EPOCH
      clock.updateTime(now)
      for
        storedFreddie <- f.dao.store(freddie).single
        coll <- f.collection
        values <- coll.find(BsonDocument().asJava).many
      yield
        storedFreddie._id shouldBe defined
        storedFreddie.dateCreated shouldEqual Some(now)
        storedFreddie.lastUpdated shouldEqual Some(now)
        storedFreddie.firstName shouldEqual "Freddie"
        storedFreddie.surname shouldEqual "Mercury"
        values should contain theSameElementsAs Seq(storedFreddie)
  }

  "Storing an element that already has an id" should {
    "populate its last updated date and not create a new element" in: f =>
      val now: Instant = Instant.EPOCH
      val later: Instant = now.plusSeconds(60)
      clock.updateTime(now)
      for
        freddieMercury <- f.dao.store(freddie).single
        freddieBulsara <-
          clock.updateTime(later)
          f.dao.store(freddieMercury.copy(surname = bulsara)).single
        coll <- f.collection
        values <- coll.find(BsonDocument().asJava).many
      yield
        freddieBulsara._id shouldEqual freddieMercury._id
        freddieBulsara.dateCreated shouldEqual Some(now)
        freddieBulsara.lastUpdated shouldEqual Some(later)
        freddieBulsara.firstName shouldEqual freddie.firstName
        freddieBulsara.surname shouldEqual bulsara
        values should contain theSameElementsAs Seq(freddieBulsara)
  }

  "Finding all elements" should {
    "find all stored elements" in: f =>
      for
        storedFreddie <- f.dao.store(freddie).single
        storedBrian <- f.dao.store(brian).single
        values <- f.dao.findAll().many
      yield values should contain theSameElementsAs Seq(
        storedFreddie,
        storedBrian
      )
  }

  "Finding some elements" should {
    "find all the queen members born after 1948" in: f =>
      import f.dao.*
      val birthInstant: Instant = January(1, 1948) at 12.am
      for
        _ <- f.dao.store(freddie).single
        _ <- f.dao.store(brian).single
        storedRoger <- f.dao.store(roger).single
        storedJohn <- f.dao.store(john).single
        youngsters <- f.dao.findWhere("birthInstant" >= birthInstant).many
      yield youngsters should contain theSameElementsAs Seq(
        storedJohn,
        storedRoger
      )
  }

  "Finding an element by ID" should {
    "find the element when it exists" in: f =>
      for
        storedFreddie <- f.dao.store(freddie).single
        _ <- f.dao.store(brian).single
        maybeFreddie <- f.dao
          .findById(storedFreddie._id.get)
          .maybe
      yield maybeFreddie shouldEqual Some(storedFreddie)

    "not find the element when it does not exist" in: f =>
      for
        _ <- f.dao.store(freddie).single
        _ <- f.dao.store(brian).single
        maybeFreddie <- f.dao.findById(invalidId).maybe
      yield maybeFreddie shouldEqual None
  }

  "Finding one element" should {
    "find nothing when nothing matches" in: f =>
      import f.dao.*
      for
        _ <- f.dao.store(freddie).single
        _ <- f.dao.store(brian).single
        maybeValue <- f.dao
          .findOne("surname" === roger.surname)
          .maybe
      yield maybeValue shouldBe empty

    "find the element when it exists" in: f =>
      import f.dao.*
      for
        storedFreddie <- f.dao.store(freddie).single
        _ <- f.dao.store(brian).single
        maybeValue <- f.dao
          .findOne("surname" === freddie.surname)
          .maybe
      yield maybeValue shouldEqual Some(storedFreddie)
  }

  "Finding some elements" should {
    "create a source that contains all elements that match the predicate" in:
      f =>
        import f.dao.*
        for
          storedFreddie <- f.dao.store(freddie).single
          storedBrian <- f.dao.store(brian).single
          _ <- f.dao.store(roger).single
          values <- f.dao.findWhere("surname" !== roger.surname).many
        yield values should contain theSameElementsAs Seq(
          storedFreddie,
          storedBrian
        )
  }

  "Updating an element" should {
    "do nothing if the query is not matched" in: f =>
      import f.dao.*
      for
        storedFreddie <- f.dao.store(freddie).single
        freddieBulsara <- f.dao
          .update(
            "surname" === brian.surname,
            m => m.copy(surname = bulsara)
          )
          .maybe
        values <- f.dao.findAll().many
      yield
        freddieBulsara shouldBe None
        values should contain theSameElementsAs Seq(storedFreddie)

    "update the element if it is matched" in: f =>
      import f.dao.dsl.*
      val now: Instant = Instant.EPOCH
      val later: Instant = now.plusSeconds(60)

      for
        freddieMercury <- f.dao.store(freddie).single
        freddieBulsara <- {
          clock.updateTime(later)
          f.dao
            .update(
              "surname" === freddie.surname,
              m => m.copy(surname = bulsara)
            )
            .maybe
        }
        values <- f.dao.findAll().many
      yield
        freddieBulsara shouldEqual (
          Some(
            freddieMercury.copy(surname = bulsara, lastUpdated = Some(later))
          )
        )
        values should contain theSameElementsAs freddieBulsara.toSeq
  }

  "Upserting an element" should {
    "insert a new element if the query is not matched" in: f =>
      import f.dao.dsl.*
      val now: Instant = Instant.EPOCH
      clock.updateTime(now)
      for
        freddieBulsara <- f.dao
          .upsert(
            "surname" === freddie.surname,
            m =>
              m.copy(
                firstName = farrokh,
                surname = bulsara,
                birthInstant = September(5, 1946) at 1.am
              )
          )
          .single
        values <- f.dao.findAll().many
      yield
        freddieBulsara shouldEqual Musician(
          _id = freddieBulsara._id,
          firstName = farrokh,
          surname = bulsara,
          birthInstant = September(5, 1946) at 1.am,
          lastUpdated = Some(now),
          dateCreated = Some(now)
        )
        values should contain theSameElementsAs Seq(freddieBulsara)

    "update the element if it is matched" in: f =>
      import f.dao.dsl.*
      val now: Instant = Instant.EPOCH
      val later: Instant = now.plusSeconds(60)

      for
        freddieMercury <- f.dao.store(freddie).single
        farrokhBulsara <-
          clock.updateTime(later)
          f.dao
            .upsert(
              "surname" === freddie.surname,
              m => m.copy(firstName = farrokh, surname = bulsara)
            )
            .single
        values <- f.dao.findAll().many
      yield
        farrokhBulsara shouldEqual (
          freddieMercury.copy(
            firstName = farrokh,
            surname = bulsara,
            lastUpdated = Some(later)
          )
        )
        values should contain theSameElementsAs Seq(farrokhBulsara)
  }

  "removing elements" should {
    "remove all the items that satisfy the predicate" in: f =>
      import f.dao.dsl.*
      for
        _ <- f.dao.store(freddie).single
        johnDeacon <- f.dao.store(john).single
        count <- f.dao.removeWhere("surname" === freddie.surname).single
        values <- f.dao.findAll().many
      yield
        values should contain theSameElementsAs Seq(johnDeacon)
        count shouldEqual (1)
  }

  override def createDao(
      eventualDatabase: Future[MongoDatabase],
      clock: Clock
  ): MusicianMongoDbDao =
    new MusicianMongoDbDao(eventualDatabase, clock)
