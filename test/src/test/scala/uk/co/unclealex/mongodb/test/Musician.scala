package uk.co.unclealex.mongodb.test

import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.mongodb.persistable.{
  HasDateCreated,
  HasLastUpdated,
  Persistable,
  Upsertable
}

import java.time.Instant

case class Musician(
    _id: Option[ID],
    firstName: String,
    surname: String,
    birthInstant: Instant,
    lastUpdated: Option[Instant],
    dateCreated: Option[Instant]
) derives Persistable,
      HasLastUpdated,
      HasDateCreated

object Musician:

  def apply(
      firstName: String,
      surname: String,
      birthInstant: Instant
  ): Musician =
    Musician(
      _id = None,
      firstName = firstName,
      surname = surname,
      birthInstant = birthInstant,
      dateCreated = None,
      lastUpdated = None
    )

  given musicianIsUpsertable: Upsertable[Musician] = Upsertable(
    Musician("", "", Instant.EPOCH)
  )
