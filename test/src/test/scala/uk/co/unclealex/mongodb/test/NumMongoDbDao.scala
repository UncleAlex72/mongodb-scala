package uk.co.unclealex.mongodb.test

import com.mongodb.reactivestreams.client.MongoDatabase
import org.apache.pekko.actor.ActorSystem
import uk.co.unclealex.mongodb.bson.BsonCodec
import uk.co.unclealex.mongodb.dao.{Index, MongoDbDao, Sort}
import uk.co.unclealex.mongodb.persistable.Persistable

import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}

class NumMongoDbDao(
    private val eventualDatabase: Future[MongoDatabase],
    private val clock: Clock
)(using
    ec: ExecutionContext,
    persistable: Persistable[Num],
    actorSystem: ActorSystem
) extends MongoDbDao[Num](eventualDatabase, clock, "num"):

  override val indicies: Seq[Index[Num]] = Seq(
    index.on("num").ascending.named("num_index")
  )
  override val defaultSort: Option[Sort] = "num".asc
