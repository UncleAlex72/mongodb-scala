package uk.co.unclealex.mongodb.test

import com.mongodb.reactivestreams.client.MongoDatabase
import org.apache.pekko.actor.ActorSystem
import uk.co.unclealex.mongodb.dao.{Index, MongoDbDao}
import uk.co.unclealex.mongodb.persistable.Persistable

import java.time.Clock
import scala.concurrent.{ExecutionContext, Future}

class MusicianMongoDbDao(
    val eventualDatabase: Future[MongoDatabase],
    val clock: Clock
)(using
    ec: ExecutionContext,
    persistable: Persistable[Musician],
    actorSystem: ActorSystem
) extends MongoDbDao[Musician](eventualDatabase, clock, "musician"):

  override val indicies: Seq[Index[Musician]] = Seq(
    index.on("surname").ascending.named("index_name")
  )
