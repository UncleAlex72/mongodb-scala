package uk.co.unclealex.mongodb.test

import uk.co.unclealex.mongodb.bson.ID
import uk.co.unclealex.mongodb.persistable.Persistable

case class Num(_id: Option[ID] = None, num: Option[Int]) derives Persistable
