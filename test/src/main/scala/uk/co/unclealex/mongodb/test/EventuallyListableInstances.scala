package uk.co.unclealex.mongodb.test

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.{Sink, Source}
import org.reactivestreams.Publisher

import scala.concurrent.Future

trait EventuallyListableInstances:

  type ListableSource[A] = Source[A, ?]

  given [A](using ActorSystem): EventuallyListable[A, ListableSource] with
    extension (s: Source[A, ?])
      override def many: Future[Seq[A]] = s.runWith(Sink.seq)
      override def single: Future[A] = s.runWith(Sink.head)
      override def maybe: Future[Option[A]] = s.runWith(Sink.headOption)

  given [A](using ActorSystem): EventuallyListable[A, Publisher] with
    extension (p: Publisher[A])
      override def many: Future[Seq[A]] = Source.fromPublisher(p).many
      override def single: Future[A] = Source.fromPublisher(p).single
      override def maybe: Future[Option[A]] = Source.fromPublisher(p).maybe

object EventuallyListableInstances extends EventuallyListableInstances
