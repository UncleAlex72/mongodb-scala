package uk.co.unclealex.mongodb.test

import org.apache.pekko.actor.ActorSystem
import org.apache.pekko.stream.scaladsl.{Sink, Source}
import org.reactivestreams.Publisher

import scala.concurrent.Future

trait EventuallyListable[A, S[_]]:
  extension (as: S[A])
    def many: Future[Seq[A]]
    def single: Future[A]
    def maybe: Future[Option[A]]
