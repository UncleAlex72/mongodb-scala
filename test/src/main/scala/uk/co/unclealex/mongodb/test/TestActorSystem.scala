package uk.co.unclealex.mongodb.test

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.pekko.actor.ActorSystem
import org.scalatest.{BeforeAndAfterAll, Suite}

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

trait TestActorSystem extends BeforeAndAfterAll { this: Suite =>

  val config: Config = ConfigFactory.defaultReference()

  given actorSystem: ActorSystem =
    ActorSystem("test", config)

  override def afterAll(): Unit =
    Await.result(actorSystem.terminate(), 1.minute)
}
