package uk.co.unclealex.mongodb.test

import com.mongodb.reactivestreams.client.{MongoCollection, MongoDatabase}
import com.typesafe.scalalogging.StrictLogging
import de.flapdoodle.embed.mongo.distribution.Version
import de.flapdoodle.embed.mongo.transitions.{Mongod, RunningMongodProcess}
import de.flapdoodle.reverse.{StateID, TransitionWalker}
import org.apache.pekko.NotUsed
import org.apache.pekko.stream.scaladsl.{Sink, Source}
import org.bson.BsonDocument as JavaBsonDocument
import org.scalatest.matchers.should
import org.scalatest.wordspec.FixtureAsyncWordSpec
import org.scalatest.{BeforeAndAfterAll, FutureOutcome}
import uk.co.unclealex.mongodb.bson.BsonDocument.~>
import uk.co.unclealex.mongodb.bson.{BsonDocument, ID}
import uk.co.unclealex.mongodb.dao.Database.Database
import uk.co.unclealex.mongodb.dao.{MongoDatabaseFactory, MongoDbDao}

import java.io.Closeable
import java.time.Clock
import scala.compiletime.uninitialized
import scala.concurrent.Future
import scala.jdk.javaapi.CollectionConverters.asJava

abstract class MongoDbDaoBaseSpec[M, DAO <: MongoDbDao[M]](
    val collectionName: String
) extends FixtureAsyncWordSpec
    with BeforeAndAfterAll
    with TestActorSystem
    with EventuallyListableInstances
    with StrictLogging
    with should.Matchers:

  private var eventualDb: Future[Database] = uninitialized
  private var closeable: Closeable = uninitialized

  val clock = new MutableClock()

  override def beforeAll(): Unit =
    val state: TransitionWalker.ReachedState[RunningMongodProcess] = Mongod
      .instance()
      .transitions(Version.Main.V6_0)
      .walker()
      .initState(StateID.of(classOf[RunningMongodProcess]))
    val (_, _eventualDb) = MongoDatabaseFactory(
      s"mongodb://localhost:${state.current().getServerAddress.getPort}/test"
    )
    eventualDb = _eventualDb
    closeable = () => state.current().stop()

  case class DaoAndDatabaseProvider(
      dao: DAO,
      bsonCollection: Future[MongoCollection[BsonDocument]],
      collection: Future[MongoCollection[M]],
      eventualDatabase: Future[Database]
  )

  def initialData(): Seq[BsonDocument] = Seq.empty

  type FixtureParam = DaoAndDatabaseProvider

  def withFixture(test: OneArgAsyncTest): FutureOutcome =
    val _initialData = initialData()
    val eventualPopulatedDb =
      val initialDataWithIds: Seq[BsonDocument] = _initialData.map: doc =>
        if (doc.get("_id").isDefined) doc
        else doc.merge(BsonDocument("_id" ~> ID()))
      val populatedDbSource: Source[JavaBsonDocument, NotUsed] =
        for
          db <- Source.future(eventualDb)
          collection = db.getCollection(
            collectionName,
            classOf[JavaBsonDocument]
          )
          deleteResult <- Source.fromPublisher(
            collection.deleteMany(new JavaBsonDocument())
          )
          insertCount <-
            if (initialDataWithIds.isEmpty)
              Source.single(0)
            else
              Source
                .fromPublisher(
                  collection.insertMany(
                    asJava(initialDataWithIds.map(_.asJava))
                  )
                )
                .map(_.getInsertedIds.entrySet().size())
          insertedDocuments <- Source.fromPublisher(collection.find())
        yield
          logger.info(s"Removed ${deleteResult.getDeletedCount} document(s)")
          logger.info(
            s"Inserted $insertCount document(s)"
          )
          insertedDocuments
      populatedDbSource
        .runForeach: doc =>
          logger.info(s"Created document $doc")
        .flatMap(_ => eventualDb)
    val dao: DAO = createDao(eventualPopulatedDb, clock)
    val param: FixtureParam = DaoAndDatabaseProvider(
      dao = dao,
      bsonCollection = dao.bsonCollection().runWith(Sink.head),
      collection = dao.collection().runWith(Sink.head),
      eventualDatabase = eventualPopulatedDb
    )
    withFixture(test.toNoArgAsyncTest(param))

  def createDao(eventualDatabase: Future[MongoDatabase], clock: Clock): DAO

  override def afterAll(): Unit = closeable.close()
