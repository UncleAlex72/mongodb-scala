package uk.co.unclealex.mongodb.test

import java.time.{Clock, Instant, ZoneId}

class MutableClock(
    private val zone: ZoneId = ZoneId.systemDefault(),
    private var now: Instant = Instant.EPOCH
) extends Clock:

  def updateTime(instant: Instant): Unit =
    now = instant

  override def getZone: ZoneId = zone

  override def withZone(zone: ZoneId): Clock =
    new MutableClock(zone, now)

  override def instant(): Instant = now
