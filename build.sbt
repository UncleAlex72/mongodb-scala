import sbt.Keys._
import sbt._
import sbtrelease.ReleasePlugin.autoImport.ReleaseTransformations._

organization := "uk.co.unclealex"
organizationName := "unclealex"

Global / onChangedBuildSource := ReloadOnSourceChanges

val _name = "mongodb-scala"
val _organization = "uk.co.unclealex"
val _scalaVersion = "3.6.2"
val _pekkoVersion = "1.1.2"

scalaVersion := _scalaVersion

def pekko(name: String): ModuleID = "org.apache.pekko" %% s"pekko-$name" % _pekkoVersion

lazy val circeVersion = "0.14.10"
lazy val reactiveMongoVersion = "1.1.0-RC14"

lazy val root = (project in file("root")).settings(
  name := _name,
  organization := _organization,
  version := version.in(ThisBuild).value,
  libraryDependencies ++= Seq(
    "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
    "uk.co.unclealex" %% "string-like" % "2.0.4",
    pekko("stream"),
    pekko("actor"),
    "org.scalatest" %% "scalatest" % "3.2.19" % Test,
    "org.mongodb" % "mongodb-driver-reactivestreams" % "5.3.1",
    "org.typelevel" %% "shapeless3-deriving" % "3.4.3"
  ) ++ Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % circeVersion),
  scalaVersion := _scalaVersion,
  organizationHomepage := Some(url("https://bitbucket.org/UncleAlex72/")),
  scmInfo := Some(
    ScmInfo(
      url("https://bitbucket.org/UncleAlex72/mongodb-scala"),
      "scm:git@bitbucket.org:UncleAlex72/mongodb-scala.git"
    )
  ),
  developers := List(
    Developer(
      id = "1",
      name = "Alex Jones",
      email = "alex.jones@unclealex.co.uk",
      url = url("https://bitbucket.org/UncleAlex72/")
    )
  ),
  description := "Useful base libraries for using MongoDB with Scala.",
  licenses := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt")),
  homepage := Some(url("https://bitbucket.org/UncleAlex72/mongodb-scala")),
  pomIncludeRepository := { _ => false },
  publishTo := sonatypePublishToBundle.value,
  publishMavenStyle := true,
  Test / fork := true
)

lazy val test = (project in file("./test"))
  .dependsOn(root)
  .settings(
    name := s"${_name}-test",
    organization := _organization,
    version := version.in(ThisBuild).value,
    libraryDependencies ++= Seq(
      "org.scalatest" %% "scalatest" % "3.2.19",
      "ch.qos.logback" % "logback-classic" % "1.5.15" % Test,
      "uk.co.unclealex" %% "thank-you-for-the-days" % "2.0.0" % Test,
      "com.beachape" %% "enumeratum" % "1.7.5" % Test,
      "de.flapdoodle.embed" % "de.flapdoodle.embed.mongo" % "4.6.3"
    ),
    scalaVersion := _scalaVersion,
    organizationHomepage := Some (url("https://bitbucket.org/UncleAlex72/")),
    scmInfo := Some(
      ScmInfo(
        url("https://bitbucket.org/UncleAlex72/mongodb-scala"),
        "scm:git@bitbucket.org:UncleAlex72/mongodb-scala.git"
      )
    ),
    developers := List(
      Developer(
        id = "1",
        name = "Alex Jones",
        email = "alex.jones@unclealex.co.uk",
        url = url("https://bitbucket.org/UncleAlex72/")
      )
    ),
    description := "Useful base libraries for using MongoDB with Scala.",
    licenses := List("Apache 2" -> new URL("http://www.apache.org/licenses/LICENSE-2.0.txt")),
    homepage := Some(url("https://bitbucket.org/UncleAlex72/mongodb-scala")),
    pomIncludeRepository := { _ => false },
    publishTo := sonatypePublishToBundle.value,
    publishMavenStyle := true,
    Test / fork := true
  ).enablePlugins(Sonatype)

resolvers ++= Seq(
  "Atlassian Releases" at "https://maven.atlassian.com/public/",
  Resolver.jcenterRepo
)

publishTo := sonatypePublishToBundle.value

fork in Test := true

ivyLoggingLevel := UpdateLogging.Quiet

releaseProcess := Seq[ReleaseStep](
  releaseStepCommand("scalafmtCheckAll"),
  checkSnapshotDependencies, // : ReleaseStep
  inquireVersions, // : ReleaseStep
  runTest, // : ReleaseStep
  setReleaseVersion, // : ReleaseStep
  commitReleaseVersion, // : ReleaseStep, performs the initial git checks
  tagRelease,
  releaseStepCommand("publishLocal"),
  releaseStepCommand("publishSigned"),
  releaseStepCommand("sonatypeBundleRelease"),
  setNextVersion, // : ReleaseStep
  commitNextVersion, // : ReleaseStep
  pushChanges // : ReleaseStep, also checks that an upstream branch is properly configured
)
